#include <QVector>
#include <QDebug>
#include <ctime>

//! Сохраняет список индексов удаления/вставки/изменения,
//! которые затем можно применить к исходной последовательности (файловому хранилищу)
//! чтобы достроить ее до текущей (кэшу в памяти)
class Indexies {
public:
    void markChanged(const size_t indexChange) {
        if (std::find(m_insertIndexies.cbegin(),m_insertIndexies.cend(), indexChange) == m_insertIndexies.cend() &&
                std::find(m_changeIndexies.cbegin(), m_changeIndexies.cend(), indexChange) == m_changeIndexies.cend()) {
            m_changeIndexies.push_back(indexChange);
        }
    }

    void markInserted(const size_t indexInsert) {
        for (size_t s=0; s<m_insertIndexies.size(); ++s) {
            if (m_insertIndexies.at(s) >= indexInsert) {
                ++m_insertIndexies[s];
            }
        }
        for (size_t s=0; s<m_changeIndexies.size(); ++s) {
            if (m_changeIndexies.at(s) >= indexInsert) {
                ++m_changeIndexies[s];
            }
        }

        auto rIndexIt = std::find(m_removeIndexies.cbegin(), m_removeIndexies.cend(), indexInsert);
        if (rIndexIt != m_removeIndexies.cend()) {
            // remove -> insert = change
            m_removeIndexies.erase(rIndexIt);
            Q_ASSERT(std::find(m_changeIndexies.cbegin(), m_changeIndexies.cend(), indexInsert) == m_changeIndexies.cend());
            m_changeIndexies.push_back(indexInsert);
        } else {
            m_insertIndexies.push_back(indexInsert);
        }

        for (size_t s=0; s<m_removeIndexies.size(); ++s) {
            if (m_removeIndexies.at(s) >= indexInsert) {
                ++m_removeIndexies[s];
            }
        }
    }

    void markRemoved(const size_t indexRemove) {
        auto iIndexIt = std::find(m_insertIndexies.cbegin(), m_insertIndexies.cend(), indexRemove);
        if (iIndexIt != m_insertIndexies.cend()) {
            // insert -> remove = null
            m_insertIndexies.erase(iIndexIt);
        } else {
            m_removeIndexies.push_back(indexRemove);
        }

        auto cIndexIt = std::find(m_changeIndexies.cbegin(), m_changeIndexies.cend(), indexRemove);
        if (cIndexIt != m_changeIndexies.cend()) {
            m_changeIndexies.erase(cIndexIt);
        }

        for (size_t s=0; s<m_insertIndexies.size(); ++s) {
            Q_ASSERT(m_insertIndexies.at(s) != indexRemove);
            if (m_insertIndexies.at(s) > indexRemove) {
                --m_insertIndexies[s];
            }
        }
        for (size_t s=0; s<m_changeIndexies.size(); ++s) {
            Q_ASSERT(m_changeIndexies.at(s) != indexRemove);
            if (m_changeIndexies.at(s) > indexRemove) {
                --m_changeIndexies[s];
            }
        }
        for (size_t s=0; s<m_removeIndexies.size(); ++s) {
            if (m_removeIndexies.at(s) > indexRemove) {
                --m_removeIndexies[s];
            }
        }
    }

    void replaceRemoveInsertWithChange() {
        // заменяем одинаковые индексы вставки/удаления на индексы изменений
        for (auto it = m_insertIndexies.begin(); it != m_insertIndexies.end();) {
            auto indexInsert = *it;
            auto rIndex = std::find(m_removeIndexies.cbegin(), m_removeIndexies.cend(), indexInsert);
            if (rIndex != m_removeIndexies.cend()) {
                // remove -> insert = change
                m_removeIndexies.erase(rIndex);

                Q_ASSERT(std::find(m_changeIndexies.cbegin(), m_changeIndexies.cend(), indexInsert) == m_changeIndexies.cend());
                m_changeIndexies.push_back(indexInsert);

                it = m_insertIndexies.erase(it);
            } else {
                ++it;
            }
        }
    }

    std::vector<size_t> getInsertIndexies() const {
        return m_insertIndexies;
    }

    std::vector<size_t> getRemoveIndexies() const {
        return m_removeIndexies;
    }

    std::vector<size_t> getChangeIndexies() const {
        return m_changeIndexies;
    }

    void clearInsertIndexies() {
        m_insertIndexies.clear();
    }

    void clearRemoveIndexies() {
        m_removeIndexies.clear();
    }

    void clearChangeIndexies() {
        m_changeIndexies.clear();
    }

private:
    std::vector<size_t> m_insertIndexies;
    std::vector<size_t> m_removeIndexies;
    std::vector<size_t> m_changeIndexies;
};

//! Применение изменений к исходной последовательности
void applyChanges(const std::vector<int> &vec, std::vector<int> &origVec, Indexies &indexies)
{
    indexies.replaceRemoveInsertWithChange();

    auto insertIndexies = indexies.getInsertIndexies();
    auto removeIndexies = indexies.getRemoveIndexies();
    auto changeIndexies = indexies.getChangeIndexies();

    indexies.clearInsertIndexies();
    indexies.clearRemoveIndexies();
    indexies.clearChangeIndexies();

    if (origVec.size() + insertIndexies.size() - removeIndexies.size() != vec.size()) {
        qFatal("%s", "FAILED");
    }

    for (auto index : insertIndexies) {
        Q_ASSERT(std::find(changeIndexies.cbegin(), changeIndexies.cend(), index) == changeIndexies.cend());
    }

    std::sort(insertIndexies.begin(), insertIndexies.end());
    std::sort(removeIndexies.begin(), removeIndexies.end());

    //1 - удаляем элементы (начиная с первого)
    //если нужно удалять начиная с последнего, нужно учитывать кол-во removeIndexies перед удаляемым
    for (auto rIndex : removeIndexies) {
        //корректируем index с измененного вектора на изменяемый
        size_t insertCount = 0;
        for (auto iIndex : insertIndexies) {
            if (iIndex >= rIndex) {
                break;
            }
            ++insertCount;
        }
        if (rIndex < insertCount) {
            qFatal("%s", "ERROR");
        }
        const size_t remIndex = rIndex - insertCount;
        origVec.erase(origVec.cbegin() + static_cast<std::vector<size_t>::difference_type>(remIndex));
    }

    //2 - вставляем новые элементы
    for (auto index : insertIndexies) {
        origVec.insert(origVec.cbegin() + static_cast<std::vector<size_t>::difference_type>(index), vec.at(index));
    }

    //3 - изменяем старые элементы
    for (auto index : changeIndexies) {
        origVec[index] = vec.at(index);
    }
}

void test()
{
    Indexies indexies;

    for (uint iter=0; iter<3; ++iter) {
        qDebug() << iter << "iteration";

        srand(iter);

        //вектор, к которому непосредственно применяются изменения (кэш в памяти)
        std::vector<int> vec;

        int START_COUNT = 1000;
        for (int i=0; i<START_COUNT; ++i) {
            vec.push_back(i);
        }

        //вектор, к которому нужно будет применить изменения (файловое хранилище)
        std::vector<int> origVec = vec;

        //применяем к vec операции вставки/удаления/изменения
        int OPERATIONS_COUNT = 100000;
        for (int i=0; i<OPERATIONS_COUNT; ++i) {
            if (vec.empty() || rand()%3==0) {
                //вставка
                int newValue = START_COUNT + 1 + rand()%1000000;

                size_t indexInsert = 0;
                if (indexies.getRemoveIndexies().empty() || rand()%2) {
                    indexInsert = rand() % (vec.size()+1);
                } else {
                    indexInsert = indexies.getRemoveIndexies().at(rand() % indexies.getRemoveIndexies().size());
                }

                vec.insert(vec.cbegin() + indexInsert, newValue);
                indexies.markInserted(indexInsert);

            } else if (rand()%2) {
                //удаление
                size_t indexRemove = 0;
                if (indexies.getChangeIndexies().empty() || rand()%3==0) {
                    indexRemove = rand() % vec.size();
                } else if (indexies.getInsertIndexies().empty() || rand()%2) {
                    indexRemove = indexies.getChangeIndexies().at(rand() % indexies.getChangeIndexies().size());
                } else {
                    indexRemove = indexies.getInsertIndexies().at(rand() % indexies.getInsertIndexies().size());
                }

                vec.erase(vec.cbegin() + indexRemove);
                indexies.markRemoved(indexRemove);

            } else {
                //изменение
                int newValue = -rand()%START_COUNT - 1;
                size_t indexChange = rand() % vec.size();

                vec[indexChange] = newValue;
                indexies.markChanged(indexChange);
            }
        }

        applyChanges(vec, origVec, indexies);

        if (origVec == vec) {
            qDebug() << "SUCCESS";
        } else {
            qFatal("%s", "FAILED");
        }
    }
}

int main()
{
    test();

    return 0;
}
